<?php 
include('ressources/traitement.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Formulaire</title>
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="bootstrap/css/bootstrap-theme.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="ressources/fonctions.js"></script>
<style type="text/css">
    .formulaire{
    	margin: 20px;
    }
</style>

</head>

<div class="formulaire">
	<form method="post" onsubmit="return VerificationFormulaire(this)">
		<div class="form-group">
			<label for="champName">Votre nom</label>
			<input type="text" class="form-control" id="champName" name="champName">
		</div>
		<div class="form-group">
			<label for="champPseudo">Votre pseudo</label>
			<input type="text" class="form-control" id="champPseudo" name="champPseudo">
		</div>
		<div class="form-group">
			<label for="champEmail">Email</label>
			<input type="email" class="form-control" id="champEmail" name="champEmail">
		</div>
		<div class="form-group">
			<label for="champPassword">Votre password</label>
			<input type="password" class="form-control" id="champPassword" name="champPassword">
		</div>
		<button type="submit" name="envoi_message" class="btn btn-primary">Envoyer</button>
	</form>
</div>